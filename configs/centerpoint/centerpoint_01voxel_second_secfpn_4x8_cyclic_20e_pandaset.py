_base_ = [
    '../_base_/datasets/pandaset-3d.py',
    '../_base_/models/centerpoint_01voxel_second_secfpn_pandaset.py',
    '../_base_/schedules/cyclic_20e.py',
    '../_base_/default_runtime.py'
]

# If point cloud range is changed, the models should also change their point
# cloud range accordingly
point_cloud_range = [-51.2, -51.2, -5.0, 51.2, 51.2, 3.0]
class_names = [
    'car',
    'truck',
    'bus',
    'other_vehicle',
    'bicycle',
    'motorcycle',
    'pedestrian',
    'cone',
    'sign',
    'barrier',
    'pylon'
    ]

model = dict(
    pts_voxel_layer=dict(point_cloud_range=point_cloud_range),
    pts_bbox_head=dict(bbox_coder=dict(pc_range=point_cloud_range[:2])),
    # model training and testing settings
    train_cfg=dict(pts=dict(point_cloud_range=point_cloud_range)),
    test_cfg=dict(pts=dict(pc_range=point_cloud_range[:2]))
)

dataset_type = 'PandasetDataset'
data_root = 'data/pandaset/'
file_client_args = dict(backend='disk')

db_sampler = dict(
    data_root=data_root,
    info_path=data_root + 'pandaset_dbinfos_train.pkl',
    rate=1.0,
    prepare=dict(
        filter_by_difficulty=[-1],
        filter_by_min_points=dict(
            car=5,
            truck=5,
            bus=5,
            other_vehicle=5,
            bicycle=5,
            motorcycle=5,
            pedestrian=5,
            cone=5,
            sign=5,
            barrier=5,
            pylon=5
        )
    ),
    classes=class_names,
    sample_groups=dict(
        car=2,
        truck=3,
        bus=4,
        other_vehicle=7,
        bicycle=6,
        motorcycle=6,
        pedestrian=2,
        cone=2,
        sign=2,
        barrier=2,
        pylon=2
    ),
    points_loader=dict(
        type='LoadPointsFromFile',
        load_dim=3,
        use_dim=[0, 1, 2],
        coord_type='LIDAR'
    ),
)

train_pipeline = [
    dict(
        type='LoadPointsFromPandasetFile',
        coord_type='LIDAR'),
    dict(type='LoadAnnotations3D', with_bbox_3d=True, with_label_3d=True),
    dict(type='ObjectSample', db_sampler=db_sampler),
    dict(
        type='GlobalRotScaleTrans',
        rot_range=[-3.1416, 3.1416],
        scale_ratio_range=[0.95, 1.05],
        translation_std=[0.2, 0.2, 0.2]),
    dict(
        type='RandomFlip3D',
        sync_2d=False,
        flip_ratio_bev_horizontal=0.5,
        flip_ratio_bev_vertical=0.5),
    dict(type='PointsRangeFilter', point_cloud_range=point_cloud_range),
    dict(type='ObjectRangeFilter', point_cloud_range=point_cloud_range),
    dict(type='ObjectNameFilter', classes=class_names),
    dict(type='PointShuffle'),
    dict(type='DefaultFormatBundle3D', class_names=class_names),
    dict(type='Collect3D', keys=['points', 'gt_bboxes_3d', 'gt_labels_3d'])
]
test_pipeline = [
    dict(
        type='LoadPointsFromPandasetFile',
        coord_type='LIDAR'),
    dict(
        type='MultiScaleFlipAug3D',
        img_scale=(1333, 800),
        pts_scale_ratio=1,
        flip=False,
        transforms=[
            dict(
                type='GlobalRotScaleTrans',
                rot_range=[0, 0],
                scale_ratio_range=[1., 1.],
                translation_std=[0, 0, 0]),
            dict(type='RandomFlip3D'),
            dict(
                type='PointsRangeFilter', point_cloud_range=point_cloud_range),
            dict(
                type='DefaultFormatBundle3D',
                class_names=class_names,
                with_label=False),
            dict(type='Collect3D', keys=['points'])
        ])
]

data = dict(
    train=dict(
        type='CBGSDataset',
        dataset=dict(
            type=dataset_type,
            data_root=data_root,
            ann_file=data_root + 'pandaset_infos_train.pkl',
            pipeline=train_pipeline,
            classes=class_names,
            test_mode=False,
            # we use box_type_3d='LiDAR' in kitti nuscenes and padnaset dataset
            # and box_type_3d='Depth' in sunrgbd and scannet dataset.
            box_type_3d='LiDAR')),
    val=dict(pipeline=test_pipeline, classes=class_names),
    test=dict(pipeline=test_pipeline, classes=class_names))

evaluation = dict(interval=20)
